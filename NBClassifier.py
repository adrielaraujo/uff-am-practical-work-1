import math
import itertools
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn.metrics import confusion_matrix, accuracy_score
# from sklearn.cross_validation import  train_test_split #VERSÃO MAIS ANTIGA DO SKLEANR
from sklearn.model_selection import train_test_split #VERSÃO MAIS ATUAL DO SKLEARN


class NBClassifier():
    def __init__(self, params=None):
        self.mean_std_by_classes = {}

    def learn(self, X, y):
        feat_by_classes = {}

        # Gera um dicionario com todas as ocorrencias para cada classe >> d = {yi: [[x1],[x2], [x3], ..., [xn]]}
        for xi, yi in zip(X,y):
            if yi not in feat_by_classes:  feat_by_classes[yi] = []
            feat_by_classes[yi].append(xi)

        # Gera um dicionario contendo os valores de média e desvio padrão para cada conjunto de classee|atributo de X
        #  d = {yi: [(mean_at1, std_at1), (mean_at2, std_at2), ... , (mean_atn, std_atn)]}
        for yi, xi in feat_by_classes.items():
            self.mean_std_by_classes[yi] = [(np.mean(attribute_list), np.std(attribute_list)) for attribute_list in zip(*xi)]

    def predict(self,X):
        predictions = []

        # para cada entrada de teste, manda predizer junto com o dicionario d
        for xi in X:
            # calcula a probabilidade de cada Xi_teste considerando os dados de medias e desvio padrão por classes de X_treino
            probabilities = self.get_probabilities(xi)

            # busca a classe com maior probabilidade
            class_max_prob = max(probabilities, key=probabilities.get)
            predictions.append(class_max_prob)


        return predictions

    def get_probabilities(self, xi):
        probabilities = {}

        for yi, att_x_treino in self.mean_std_by_classes.items():
            probabilities[yi] = 1

            # para cada conjnto de std e media por classe (de X_treino), pega os valores e calcula a probabilidade junto ao X_teste[i]
            for i in range(len(att_x_treino)):
                mean, std = att_x_treino[i]
                value_xi = xi[i]

                exp_xi_mean = math.pow(value_xi - mean, 2)
                exp_std = 2* (math.pow(std, 2))
                exponent = math.exp(-(exp_xi_mean/ exp_std))

                result = (1 / (math.sqrt(2 * math.pi) * std)) * exponent

                probabilities[yi] *= result

        return probabilities

    def plot_confusion_matrix(self,cm, classes, cmap=plt.cm.Blues):
        """
        This function prints and plots the confusion matrix.
        Normalization can be applied by setting `normalize=True`.
        """
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title("Confusion Matrix")
        plt.colorbar()
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

        print('Confusion matrix')
        print(cm)

        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, cm[i, j],
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.show()



if __name__ == "__main__":
    iris_data = load_iris()
    X_train, X_test, y_train, y_test = train_test_split(iris_data['data'], iris_data['target'], test_size=0.30)
    print("xtreino:", len(X_train), "\nX_teste:", len(X_test))

    model = NBClassifier()
    model.learn(X_train,y_train)
    pred = model.predict(X_test)
    print("Acc: ", accuracy_score(y_test, pred)*100,"%")
    model.plot_confusion_matrix(confusion_matrix(y_test, pred), classes=iris_data.target_names)
    # print("Predito: ",len(a),a)
    # print("Esperado:",len(y_test), y_test)


