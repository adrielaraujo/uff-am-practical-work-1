
# coding: utf-8

# In[248]:

import itertools
import matplotlib.pyplot as plt
from scipy.spatial import distance
from sklearn.datasets import load_iris
from sklearn.metrics import confusion_matrix, accuracy_score
#from sklearn.cross_validation import  train_test_split #VERSÃO MAIS ANTIGA DO SKLEANR
from sklearn.model_selection import train_test_split #VERSÃO MAIS ATUAL DO SKLEARN
import numpy as np
import math
import seaborn


# In[249]:

iris_data = load_iris()
X_train, X_test, y_train, y_test = train_test_split(iris_data['data'], iris_data['target'], test_size = 0.30)

info_dataset()


# In[250]:

def info_dataset():
    print("Tamanho do dataset: ", len(iris_data['data']),
          "\nX Treino:", len(X_train), 
          "\ty Treino:", len(y_train), 
          "\nX Teste:", len(X_test), 
          "\ty Teste:", len(y_test))


# In[251]:

class Adriel_KnnClassifier():
    def __init__(self, K = 3):
        self.X_train, self.y_train = None, None
        self.K = K

    def learn(self, X, y):
        self.X_train = X
        self.y_train = y

    def predict(self,X):

        if self.X_train is None:
            print("Please, first call the learning method")
            return []

        dists = {}
        predicts = []

        for instancia in X:
            for i in range(len(self.X_train)):
                dists[i] = distance.euclidean(instancia,self.X_train[i])
            k_vizinhos = sorted(dists, key=dists.get)[:self.K]

            rotulos = [0,0,0]
            for indice in k_vizinhos:
                if self.y_train[indice] == 0:
                    rotulos[0] += 1
                elif self.y_train[indice] == 1:
                    rotulos[1] += 1
                else:
                    rotulos[2] +=1

            voto = rotulos.index(max(rotulos))
            predicts.append(voto)

        return predicts


# In[252]:

Knn = Adriel_KnnClassifier(K = 3)
Knn.learn(X_train,y_train)
pred_knn = Knn.predict(X_test)
desempenho_knn = accuracy_score(y_test, pred_knn)*100
print("Acc: ", desempenho_knn, "%")


# In[253]:

def plot_confusion_matrix(cm, classes, cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title("Confusion Matrix")
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

#     print('Confusion matrix')
#     print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()


# In[254]:

plot_confusion_matrix(confusion_matrix(y_test, pred_knn),iris_data.target_names)


# # Dois

# In[255]:

class Adriel_NBClassifier():
    def __init__(self, params=None):
        self.mean_std_by_classes = {}

    def learn(self, X, y):
        feat_by_classes = {}

        # Gera um dicionario com todas as ocorrencias para cada classe >> d = {yi: [[x1],[x2], [x3], ..., [xn]]}
        for xi, yi in zip(X,y):
            if yi not in feat_by_classes:  feat_by_classes[yi] = []
            feat_by_classes[yi].append(xi)

        # Gera um dicionario contendo os valores de média e desvio padrão para cada conjunto de classee|atributo de X
        #  d = {yi: [(mean_at1, std_at1), (mean_at2, std_at2), ... , (mean_atn, std_atn)]}
        for yi, xi in feat_by_classes.items():
            self.mean_std_by_classes[yi] = [(np.mean(attribute_list), np.std(attribute_list)) for attribute_list in zip(*xi)]

    def predict(self,X):
        predictions = []

        # para cada entrada de teste, manda predizer junto com o dicionario d
        for xi in X:
            # calcula a probabilidade de cada Xi_teste considerando os dados de medias e desvio padrão por classes de X_treino
            probabilities = self.get_probabilities(xi)

            # busca a classe com maior probabilidade
            class_max_prob = max(probabilities, key=probabilities.get)
            predictions.append(class_max_prob)


        return predictions

    def get_probabilities(self, xi):
        probabilities = {}

        for yi, att_x_treino in self.mean_std_by_classes.items():
            probabilities[yi] = 1

            # para cada conjnto de std e media por classe (de X_treino), pega os valores e calcula a probabilidade junto ao X_teste[i]
            for i in range(len(att_x_treino)):
                mean, std = att_x_treino[i]
                value_xi = xi[i]

                exp_xi_mean = math.pow(value_xi - mean, 2)
                exp_std = 2* (math.pow(std, 2))
                exponent = math.exp(-(exp_xi_mean/ exp_std))

                result = (1 / (math.sqrt(2 * math.pi) * std)) * exponent

                probabilities[yi] *= result

        return probabilities


# In[256]:

Nb = Adriel_NBClassifier()
Nb.learn(X_train, y_train)
pred_nb = Nb.predict(X_test)
desempenho_nb = accuracy_score(y_test, pred_nb) * 100
print("Acc: ", desempenho_nb, "%")
plot_confusion_matrix(confusion_matrix(y_test, pred_nb),iris_data.target_names)

# Comparação mesma base para versoes diferentes knn
# In[257]:

K = [3,5,7,9,11,13]
acc_knn = []
for k in K:
    Knn = Adriel_KnnClassifier({'K':k})
    Knn.learn(X_train,y_train)
    pred_knn = Knn.predict(X_test)
    desempenho_knn = accuracy_score(y_test, pred_knn)*100
    acc_knn.append(desempenho_knn)
    print("K", k, " - Acc:", desempenho_knn, "%")
#     plot_confusion_matrix(confusion_matrix(y_test, pred_knn),iris_data.target_names)
    
fig = plt.figure()
seaborn.violinplot(data=acc_knn, palette='Set2', split = True)
plt.ylabel('ACC %'); #plt.xlabel('Configuration')
plt.show()


# In[258]:

acc_nb = [desempenho_nb]*3
fig = plt.figure()
seaborn.violinplot(data=acc_nb, palette='Set2', split = True)
plt.ylabel('ACC %'); #plt.xlabel('Configuration')
plt.show()


# In[259]:

# variando os datasets


# In[270]:

acc_knn, acc_nb, acc_geral = [], [], []

for i in range(4):
    X_train, X_test, y_train, y_test = train_test_split(iris_data['data'], iris_data['target'], test_size = 0.30)

    Knn = Adriel_KnnClassifier(K = 5)
    Knn.learn(X_train,y_train)
    pred_knn = Knn.predict(X_test)
    desempenho_knn = accuracy_score(y_test, pred_knn)*100
    acc_knn.append(desempenho_knn)
    
    
    Nb = Adriel_NBClassifier()
    Nb.learn(X_train, y_train)
    pred_nb = Nb.predict(X_test)
    desempenho_nb = accuracy_score(y_test, pred_nb) * 100
    acc_nb.append(desempenho_nb)
    
    print("Randon Train/Teste[{}]".format(i))
    print("Acc KNN: ", desempenho_knn, "%")
    print("Acc NB: ", desempenho_nb, "%\n")
    
import pandas as pd
fake = pd.DataFrame({'KNN': acc_knn, 'NB': acc_nb})
seaborn.violinplot(data=fake, palette='Set2', split = True)
plt.ylabel('%ACC'); plt.xlabel('Classifiers')
plt.show()


# In[ ]:



