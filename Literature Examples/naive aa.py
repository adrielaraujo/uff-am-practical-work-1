import numpy as np
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split

class NaiveBayes():
    def __init__(self, n_clases=None):
        if n_clases is None:
            self.n_ys = None
        else:
            self.n_ys = n_clases

        self.n_features = None


        self.frec_clase = None
        self.mean = None
        self.sd = None

        self.X = None
        self.y = None

    def frecuencia(self):
        for i in self.y:
            self.frec_clase[i] += 1

    def means(self):
        accum = np.zeros((self.n_ys, self.n_features))

        i = 0
        while i < len(self.y):
            j = 0
            while j < self.n_features:
                accum[self.y[i]][j] += self.X[i][j]
                j += 1
            i += 1

        i = 0
        while i < self.n_ys:
            j = 0
            while j < self.n_features:
                self.mean[i][j] = accum[i][j] / self.frec_clase[i]
                j += 1
            i += 1

    def sds(self):
        accum = np.zeros((self.n_ys, self.n_features))

        i = 0
        while i < len(self.y):
            j = 0
            while j < self.n_features:
                accum[self.y[i]][j] += np.power(self.X[i][j] - self.mean[self.y[i]][j], 2)
                j += 1
            i += 1

        i = 0
        while i < self.n_ys:
            j = 0
            while j < self.n_features:
                self.sd[i][j] = np.sqrt(accum[i][j] / (self.frec_clase[i] - 1))
                j += 1
            i += 1

    def predict(self, X):
        result = []
        prob_y_x = np.ones(self.n_ys)

        for x in X:
            for i in range(self.n_ys):
                exp = np.exp((np.divide((self.mean[i].transpose() - x), self.sd[i]) * (self.mean[i] - x)) / (-2.0))
                prob_x_y = exp / (np.power(2 * np.pi, self.n_features / 2.0) * np.sqrt(np.linalg.norm(self.sd[i])))
                prob_y = self.frec_clase[i] / float(len(self.y))

                prob_y_x[i] = np.prod(prob_x_y) * prob_y

            result.append(np.argmax(prob_y_x))



        return result

    def learn(self, X, y):
        self.X = X
        self.y = y

        self.n_features = X.shape[1]

        if self.n_ys is None:
            self.n_ys = y.max()

        self.frec_clase = np.zeros(self.n_ys)
        self.frecuencia()

        self.mean = np.zeros((self.n_ys, self.n_features))
        self.means()

        self.sd = np.zeros((self.n_ys, self.n_features))
        self.sds()

    def acc(self, X_test, y_test):
        y_values = self.predict(X_test)
        count = 0
        for i, j in zip(y_test, y_values):
            if i == j:
                count += 1.0
        return count / len(y_test) * 100.0

if __name__ == '__main__':
    iris_data = load_iris()
    X_train, X_test, y_train, y_test = train_test_split(iris_data['data'], iris_data['target'], test_size=0.30)

    entrenador = NaiveBayes(3)
    entrenador.learn(iris_data.data, iris_data.target)
    print("Acc: " + str(entrenador.acc(iris_data.data, iris_data.target)) + "%")