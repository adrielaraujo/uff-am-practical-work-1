from scipy import *
from scipy.linalg import norm, pinv

from matplotlib import pyplot as plt


class RBF:
    def __init__(self, indim, numCenters, outdim):
        self.indim = indim
        self.outdim = outdim
        self.numCenters = numCenters
        self.centers = [random.uniform(-1, 1, indim) for i in range(numCenters)]
        self.beta = 8
        self.W = random.random((self.numCenters, self.outdim))

    def _basisfunc(self, c, d):
        d =[d]
        d = np.asarray(d)
        print("d", d, len(d), self.indim)
        aux = 4
        assert len(d[0]) == self.indim
        return exp(-self.beta * norm(c - d) ** 2)

    def _calcAct(self, X):
        # calculate activations of RBFs
        G = zeros((X.shape[0], self.numCenters), float)
        for ci, c in enumerate(self.centers):
            for xi, x in enumerate(X):
                G[xi, ci] = self._basisfunc(c, x)
        # print("G", G)
        return G

    def train(self, X, Y):
        """ X: matrix of dimensions n x indim 
            y: column vector of dimension n x 1 """
        # print("y treino", Y[:4])

        # choose random center vectors from training set
        rnd_idx = random.permutation(X.shape[0])[:self.numCenters]
        self.centers = [X[i, :] for i in rnd_idx]

        # print("center", self.centers)

        # calculate activations of RBFs
        G = self._calcAct(X)
        # print(G)

        # print("SHAPES", G.shape, Y.shape)
        # calculate output weights (pseudoinverse)
        self.W = dot(pinv(G), Y)

    def test(self, X):
        """ X: matrix of dimensions n x indim """
        # print("y teste", X[:4])

        G = self._calcAct(X)
        # print("G", G.shape)
        Y = dot(G, self.W)
        # print("Y", Y)
        return Y


if __name__ == '__main__':

    from sklearn.datasets import load_iris
    from sklearn.model_selection import train_test_split

    iris_data = load_iris()
    X_train, X_test, y_train, y_test = train_test_split(iris_data['data'], iris_data['target'], test_size=0.30)
    yy = []
    for i in y_train:
        yy.append([i])
    import numpy as np
    y_train = np.asarray(yy)

    rbf = RBF(4, 10, 1)
    print("XTrain", type(X_train), X_train.shape)
    print("YTrain", type(y_train), y_train.shape)
    print("Ytrain pos: ", y_train[:3])





    rbf.train(X_train, y_train)
    z = rbf.test(X_test)
    # print("Yt:", y_test)
    resultados = []
    for i in z:
        resultados.append(int(i[0]))
    # print("\n\nZ", z)
    print("Predito:", z[:4], "\n\nEsperado:", y_test[:4])
    print("type", type(y_test[1]), y_test[1], "\n--------------------------------------------\n")


    # ----- 1D Example ------------------------------------------------
    n = 100

    x = mgrid[-1:1:complex(0, n)].reshape(n, 1)
    # set y and add random noise
    y = sin(3 * (x + 0.5) ** 3 - 1)
    y[y>=0] = 1
    y[y<0] = -1
    # y += random.normal(0, 0.1, y.shape)

    # rbf regression
    rbf = RBF(1, 10, 1)
    print("X", type(x), x.shape)
    print("Y", type(y), y.shape)
    unique, counts = np.unique(y, return_counts=True)
    # print("len Classes", np.asarray((unique, counts)).T)
    print("Y pos: ", y[:3])

    rbf.train(x, y)
    z = rbf.test(x)
    z[z >= 0] = 1
    z[z < 0] = -1
    print("Predito:", z[:4], "\n\nEsperado:", y[:4])
    print("type", type(y[1]), y[1])





















    # plot original data
    # plt.figure(figsize=(12, 8))
    # plt.plot(x, y, 'k-')
    #
    # # plot learned model
    # plt.plot(x, z, 'r-', linewidth=2)
    #
    # # plot rbfs
    # plt.plot(rbf.centers, zeros(rbf.numCenters), 'gs')
    #
    # for c in rbf.centers:
    #     # RF prediction lines
    #     cx = arange(c - 0.7, c + 0.7, 0.01)
    #     cy = [rbf._basisfunc(array([cx_]), array([c])) for cx_ in cx]
    #     plt.plot(cx, cy, '-', color='gray', linewidth=0.2)
    #
    # plt.xlim(-1.2, 1.2)
    # plt.show()
