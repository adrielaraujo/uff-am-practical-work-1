import itertools
import matplotlib.pyplot as plt
from scipy.spatial import distance
from sklearn.datasets import load_iris
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split #VERSÃO MAIS ATUAL DO SKLEARN
from numpy import arange
from NBClassifier import NBClassifier
from KnnClassifier import KnnClassifier
import seaborn

def plot_confusion_matrix(cm, classes, cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title("Confusion Matrix")
    tick_marks = arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    print('Confusion matrix')
    print(tick_marks)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    # plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    # plt.show()



if __name__ == "__main__":

    iris_data = load_iris()
    X_train, X_test, y_train, y_test = train_test_split(iris_data['data'], iris_data['target'], test_size = 0.30)

    print("Tamanho do dataset: ", len(iris_data['data']),
          "\nX Treino:", len(X_train),
          "\ty Treino:", len(y_train),
          "\nX Teste:", len(X_test),
          "\ty Teste:", len(y_test))

    print("\n-----------------------------------------\nKNN Classifier")

    K = [3,5,7,9,11,13]
    acc_knn = []
    for k in K:
        Knn = KnnClassifier({'K':k})
        Knn.learn(X_train,y_train)
        pred_knn = Knn.predict(X_test)
        desempenho_knn = accuracy_score(y_test, pred_knn)*100
        acc_knn.append(desempenho_knn)
        print("Acc: ", desempenho_knn, "%")
        plot_confusion_matrix(confusion_matrix(y_test, pred_knn),iris_data.target_names)


    print("\n-----------------------------------------\nNaive Bayes Classifier")
    Nb = NBClassifier()
    Nb.learn(X_train, y_train)
    pred_nb = Nb.predict(X_test)
    desempenho_nb = accuracy_score(y_test, pred_nb) * 100
    print("Acc: ", desempenho_nb, "%")
    plot_confusion_matrix(confusion_matrix(y_test, pred_nb),iris_data.target_names)
    acc_nb = [desempenho_nb]*3

    acc_geral = [acc_knn,acc_nb]

    fig = plt.figure()
    seaborn.violinplot(data=acc_geral, palette='Set2', split = True)
    plt.ylabel('ACC%')
    plt.show()