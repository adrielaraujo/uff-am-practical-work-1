import itertools
import matplotlib.pyplot as plt
from scipy.spatial import distance
from sklearn.datasets import load_iris
from sklearn.metrics import confusion_matrix, accuracy_score
# from sklearn.cross_validation import  train_test_split #VERSÃO MAIS ANTIGA DO SKLEANR
from sklearn.model_selection import train_test_split #VERSÃO MAIS ATUAL DO SKLEARN
from numpy import arange
from NBClassifier import NBClassifier

class KnnClassifier():
    def __init__(self, params=None):
        self.X_train, self.y_train = None, None
        self.K = params['K']

    def learn(self, X, y):
        self.X_train = X
        self.y_train = y

    def predict(self,X):

        if self.X_train is None:
            print("Please, first call the learning method")
            return []

        dists = {}
        predicts = []

        for instancia in X:
            for i in range(len(self.X_train)):
                dists[i] = distance.euclidean(instancia,self.X_train[i])
            k_vizinhos = sorted(dists, key=dists.get)[:self.K]

            rotulos = [0,0,0]
            for indice in k_vizinhos:
                if self.y_train[indice] == 0:
                    rotulos[0] += 1
                elif self.y_train[indice] == 1:
                    rotulos[1] += 1
                else:
                    rotulos[2] +=1
            voto = rotulos.index(max(rotulos))
            predicts.append(voto)

        return predicts






